# Atelier Tech 

https://atelier-tech.gitlab.io/jekyll/

This repository is a [Jekyll](https://jekyllrb.com/) project.

For local development you can just run `docker-compose up` and open you browser in http://0.0.0.0:4000/jekyll/.


# FAQ

1. How can I add myself on the collaborators session?

    It is simple, fork the repository, add a markdown file inside `/_people/` and open a merge request to the main branch. 
